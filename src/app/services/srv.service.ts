import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';

@Injectable({
  providedIn: 'root'
})
export class SrvService {

  constructor(
  	private http: HttpClient,
  	private router: Router,
    public storage: Storage,
    private uniqueDeviceID: UniqueDeviceID
  ) { }

  public urlApi = "http://127.0.0.1:8000/api/" //"https://abelcoo.000webhostapp.com/api/"
  public url = "https://abelcoo.000webhostapp.com/"

  getData(link) {
  	return this.http.get(link)
  }

  postData(link, data) {
  	return this.http.post(link, data)
  }

  updateData(link, data) {
  	return this.http.put(link, data)
  }

  deleteData(link) {
  	return this.http.delete(link)
  }

  redirectTo(url_router) {
  	return this.router.navigate([url_router])
  }

  getLocal(key) {
    this.storage.get(key).then((val) => {
      return val;
    });
  }

  setLocal(key, val) {
    this.storage.set(key, val).then(function () {
      return key;
    });
  }

  clearLocalAll() {
    this.storage.clear();
  }

  getPhoneID() {
    return this.uniqueDeviceID.get()
            .then((uuid: any) => console.log(uuid))
            .catch((error: any) => console.log(error));
  }
}
