import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SrvService} from '../services/srv.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  constructor(
    protected router: Router,
    private srv: SrvService
  ) { }

  logout() {
    this.srv.postData(this.srv.urlApi+'auth/logout', {
      'user_unique_name': this.srv.getLocal('user_unique_name')
    });
    this.srv.clearLocalAll();
    window.location.href='login'
  }

}
