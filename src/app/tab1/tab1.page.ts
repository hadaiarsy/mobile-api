import { Component } from '@angular/core';
import { SrvService } from '../services/srv.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  constructor(
  		private srv: SrvService
  	) {}

  getJadwal_id: any;
  hari: any;
  data_jadwal: any;
  link_api: any;
  user_id: any;

  ngOnInit() {
    this.user_id = this.srv.getLocal("user_id");

    this.hari = "senin";
    this.srv.getData(this.srv.urlApi+"jadwal/1/senin").subscribe(
      data => {
        console.log(data);
        this.data_jadwal = data;
        this.data_jadwal = this.data_jadwal.serve;
      }
    )
  }

}
