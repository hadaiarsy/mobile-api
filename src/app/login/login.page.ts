import { Component, OnInit } from '@angular/core';
import { ModalController, NavController} from '@ionic/angular';
import { SrvService } from '../services/srv.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  username: any;
  password: any;
  loginAct: any;
  user_id: any;
  phone_id = this.srv.getPhoneID();

  constructor(
    protected router: Router,
    private modalController: ModalController,
    private navCtrl: NavController,
    private srv: SrvService
  ) { }

  ngOnInit() {
    this.user_id = this.srv.getLocal('user_id')
    console.log(this.user_id)
    if(this.user_id) {
      this.srv.redirectTo('/tabs')
    }
  }

  login() {
    this.srv.postData(this.srv.urlApi+"auth/login", {
      "username": this.username,
      "password": this.password,
    }).subscribe(
      data=>{
        this.loginAct = data
        if(this.loginAct.message == "SUKSES.") {
          console.log(this.loginAct.serve[0])
          this.username = '';
          this.password = '';
          console.log(this.loginAct.user_id)
          this.user_id = this.srv.setLocal('user_id', this.loginAct.user_id);
          console.log(this.user_id)
          console.log(this.user_id)
          if(this.loginAct.user_id == 'guru') {
            this.srv.setLocal('mapel_id', this.loginAct.serve[0].mapel_id)
            this.srv.setLocal('kode_guru', this.loginAct.serve[0].kode_guru)
          }else if(this.loginAct.user_id == 'murid') {
            this.srv.setLocal('kelas_id', this.loginAct.serve[0].kelas_id)
            this.srv.setLocal('nis', this.loginAct.serve[0].nis)
          }
          this.srv.redirectTo('/tabs')
        } else {
          console.log(data)
        }
      }
    )
  }

}
