import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SrvService } from '../services/srv.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  constructor(
    private router: Router,
    private srv: SrvService
  ) { }

  user_id: any;
  notifCheck_id: any;
  guru_id: any;
  kelas_id: any;
  data_notif: any;

  ngOnInit() {
  	this.user_id = this.srv.getLocal("user_id");
	this.srv.getData(this.srv.urlApi+"notifikasi/1").subscribe(
		data => {
			console.log(data);
			this.data_notif = data;
			this.data_notif = this.data_notif.serve;
		}
	)
  }

  goCreate() {
    this.srv.redirectTo('create-notification');
  }

}
