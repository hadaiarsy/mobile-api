import { Component } from '@angular/core';
import { SrvService } from '../services/srv.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor(private srv: SrvService) {}

  user_id: any;
  mapel_id: any;

  ngOnInit() {
  	this.user_id = this.srv.getLocal('user_id')
  	if(this.user_id != 'guru') {
  		// this.srv.redirectTo('login')
  	}
  }

}
