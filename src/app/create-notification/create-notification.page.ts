import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-notification',
  templateUrl: './create-notification.page.html',
  styleUrls: ['./create-notification.page.scss'],
})
export class CreateNotificationPage implements OnInit {

  constructor(
    protected router: Router
  ) { }

  ngOnInit() {
  }

  addNotif(data) {

  }

  back() {
    this.router.navigateByUrl('/tabs/tab2');
  }

}
